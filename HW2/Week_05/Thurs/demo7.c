#include "demo7.h"
#include <demo_util.h>
#include <stdio.h>
#include <mpi.h>
#include <math.h>

#define PI 3.141592653589793116

/* Integrate this over [0,1] */
double f(double x)
{
    double fx;

    //fx = exp(x)*pow(sin(2*PI*x),2);
    fx = exp(-x*x)*pow((x-1),2);
    //fx = x*x;
    return fx;
}


void main(int argc, char** argv)
{
    /* Data arrays */
    double n_global;
    double local_bins;
    double step_size;
    double range[2];
    /* MPI variables */
    int my_rank, nprocs;

    MPI_Init(&argc, &argv);

    MPI_Comm_rank(MPI_COMM_WORLD, &my_rank);
    set_rank(my_rank);  /* Used in printing */

    MPI_Comm_size(MPI_COMM_WORLD, &nprocs);

    if (my_rank == 0)
    {        
        int p0;
        read_int(argc,argv, "-p",&p0);
        n_global = pow2(p0);     /* Number of sub-intervals used for integration */

        /* Hardwire values */
        double a,b;
        a = -1;
        b = 1;  

        /* Your Node P=0 work goes here */
        /* Send sub-interval to other processors */
     
        double w = (b-a)/nprocs;
	
        int p;
        for(p = 1; p < nprocs; p++)
        {
            /* pass two values to processor p */
	    
            range[0] = a+p*w;
            range[1] = range[0] + w;

            int tag = 0;
            int dest = p;
            MPI_Send((void*) range,2,MPI_DOUBLE,dest,tag,MPI_COMM_WORLD);

        }
        //Set the limits for proc 0
	range[0]=a;
	range[1]=a+w;
    }
    else
    {
        MPI_Status status;
        int count;
        /* Receive range values */
        int source = 0;
        int tag = 0;
        MPI_Recv((void*) range,2,MPI_DOUBLE,source,tag,MPI_COMM_WORLD,&status);  
		
        MPI_Get_count(&status,MPI_DOUBLE,&count);         
    }
    
	
    /* Broadcast value of N to all processors;  compute number of panels in each 
    subinterval */
	MPI_Bcast(&n_global, 1, MPI_DOUBLE, 0, MPI_COMM_WORLD);

    /* Every processor now knows its range and number of panels */
	local_bins = n_global/nprocs;

	step_size = (range[1]-range[0])/local_bins;
    /* Apply trapezoidal rule (for loop) */
	//print_debug("%f,%f",range[0],range[1]);
	int local_i;
	double local_integral;
	double local_x;
	local_integral=-(f(range[0])+f(range[1]))/2;
	double total_sum;

	for(local_i = 0; local_i < local_bins; local_i++)
	{
		local_x=range[0]+step_size*local_i;
		local_integral=local_integral+f(local_x);
	}
	local_integral = local_integral*step_size;
    /* Call MPI_Reduce to get final integral */

	MPI_Reduce(&local_integral,&total_sum,1,MPI_DOUBLE,MPI_SUM,0,MPI_COMM_WORLD); 
	double exact = 1.872592957265838754602878538234098148617687929406051152575;
	print_global("%.19f %.19f %.19g\n",total_sum,exact,exact-total_sum);
	

    MPI_Finalize();

}
