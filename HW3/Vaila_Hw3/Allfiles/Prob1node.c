#include "hmwk3.h"
#include <demo_util.h>
# include <stdio.h>
#include <mpi.h>
#include <math.h>
#include <unistd.h>
#define PI 3.14159265358979323846264338327


double utrue(double x)
{
    double u;
    double pi2;
    pi2 = 2*PI;
    u = cos(pi2*x);
    return u;
}

double rhs(double x)
{
    double fx;
    double pi2;
    pi2 = 2*PI;
    fx = -(pi2)*(pi2)*cos(pi2*x);
    return fx;
}


void main(int argc, char** argv)
{
    /* Data arrays */
    double a,b;
    int n_global;
    int n_local;
    double h;
    double range[2];
    double *x, *F, *B;

    /* Iterative variables */
    double tol;
    int kmax;

    /* Misc variables */
    int i,j,k;

    /* MPI variables */
    int my_rank, nprocs;

    /* ----------------------------------------------------------------
       Set up MPI
     ---------------------------------------------------------------- */

    MPI_Init(&argc, &argv);

    MPI_Comm_rank(MPI_COMM_WORLD, &my_rank);
    set_rank(my_rank);  /* Used in printing */
    read_loglevel(argc,argv);

    MPI_Comm_size(MPI_COMM_WORLD, &nprocs);

    /* ----------------------------------------------------------------
       Read parameters from the command line
     ---------------------------------------------------------------- */
    if (my_rank == 0)
    {        
        int m,err,loglevel;
        read_int(argc,argv, "-m", &m, &err);
        if (err > 0)
        {
            print_global("Command line argument '-m' not found\n");
            exit(0);
        }        

        read_int(argc,argv, "-kmax", &kmax, &err);
        if (err > 0)
        {
            print_global("Command line argument '--kmax' not found\n");
            exit(0);
        }

        read_double(argc,argv, "-tol", &tol, &err);
        if (err > 0)
        {
            print_global("Command line argument '--tol' not found\n");
            exit(0);
        }
        //print_global("%g\n",tol);
        n_global = pow2(m);     

        /* Hardwire domain values values */
        a = 0;
        b = 1; 

    /* Your Node P=0 work goes here */
        /* Send sub-interval to other processors */
     
        double w = (b-a)/nprocs;
    
        int p;
        for(p = 1; p < nprocs; p++)
        {
            /* pass two values to processor p */
        
            range[0] = a+p*w;
            range[1] = range[0] + w;

            int tag = 0;
            int dest = p;
            MPI_Send((void*) range,2,MPI_DOUBLE,dest,tag,MPI_COMM_WORLD);

        }
        //Set the limits for proc 0
        range[0]=a;
        range[1]=a+w; 
    }

    else
    {
        MPI_Status status;
        int count;
        /* Receive range values */
        int source = 0;
        int tag = 0;
        MPI_Recv((void*) range,2,MPI_DOUBLE,source,tag,MPI_COMM_WORLD,&status);  
        
        MPI_Get_count(&status,MPI_DOUBLE,&count);         
    }
    
    //print_debug("%f %f\n",range[0],range[1]); 

    /* ---------------------------------------------------------------
       Broadcast global information : kmax, tol, n_global
         -- compute range, h, and number of intervals for each
            processor
    --------------------------------------------------------------- */

    MPI_Bcast(&n_global, 1, MPI_INT, 0, MPI_COMM_WORLD);
    MPI_Bcast(&kmax, 1, MPI_INT, 0, MPI_COMM_WORLD);
    MPI_Bcast(&tol, 1, MPI_DOUBLE, 0, MPI_COMM_WORLD);
    n_local = n_global/nprocs;
    //print_debug("%d\n",n_local);
    h = (range[1]-range[0])/n_local;
    double *u;
    double *w;
    int start;
    int end;
    zeros_array(2,&w);
    if(my_rank!=0 && my_rank!=nprocs-1)
    {
        zeros_array(n_local+3,&u);    
    }
    else
    {
        zeros_array(n_local+2,&u);
    }
                                 // INTERMEDIATE PROCESSORS SHOULD HAVE ONE MORE VALUE THAN THE PROCESSORS ON THE EDGES.
    if(my_rank==0)             // Suppose there are 16 points placed equally with h=1/16
                                //X REFERS TO VALUES(SOLUTIONS AT THAT POINTS) THAT ARE TO BE OBTAINED FROM NEX/PREV PROCESORS
    {                          // 0  1/16  2/16  3/16  4/16  X  =====> PROC0 SHOULD HAVE 6 ENTRIES
      u[0] = 1;                // |                     |                  |                      |                        |
    }                         //                  X   4/16 5/16 6/16  7/16 8/16   X ================> PROC1 SHOULD HAVE 7 ENTRIES
                               //                                      X   8/16 9/16 10/16 11/16 12/16   X ========> PROC2 SHOULD HAVE 7 ENTRIES
    if(my_rank==nprocs-1)      //                                                             X  12/16 13/16 14/16 15/16 16/16 ========> PROC3 SHOULD HAVE 6 ENTRIES
    {                       
        u[n_local+1] = 1;  
    }
    
    int iter;
    double u_prec, diff, largest_diff_global, largest_diff_local, ri, xis;
    
     for(iter=0; iter<kmax; iter++)
    {
        largest_diff_local = 0;
        u_prec = u[0];
        if(my_rank==0)
        {
            start=1;
            xis = range[0]+h;
            end = n_local+1;
        }
        else if(my_rank==nprocs-1)
        {
            start=1;
            xis = range[0];
            end = n_local+1;
        }
        else if(my_rank!=0 && my_rank!=nprocs-1)
        {
            start=1;
            xis = range[0];
            end=n_local+2;
        }
         //ITERATIVE JACOBIAN 
        for(i=start; i<end; i++)
        {
            //print_global("%d",i);
            ri = - h*h*rhs(xis) - (-u_prec + 2*u[i] -u[i+1]);
            u_prec = u[i];  //HOLD THE PRECEDING U value to u[1], u[2]...so on. NOT UPDATED U[I] VALUES.
            u[i] = u[i]+0.5*ri;   // UPDATE THE U VALUE BASED ON THE RESIDUAL.
            diff = fabs(u[i] - u_prec); // THIS TAKES THE DIFFERENCE BETWEEN SUCCESSIVE U VALUES, BASICALLY IT'S RESIDUAL!!
                                        // NOTE THAT CONVERGENCE CONDITION IS AN INFINITY NORM.
            if(diff>largest_diff_local) // WHICH MEANS THAT THE MAXIMUM OF ALL DIFFERENCES BETWEEN 2 SUCCESSIVE Us HAS TO BE  
             {                          // LESS THAN EPSILON.
             largest_diff_local = diff;
             }
            xis += h;
        }
 /* 
    COMMUNICATIONS ARE DONE LIKE THIS: THE POROCESORS ON THE END WILL HAVE TO DO 1 SEND AND 1 RECEIVE.

    THE INTERNAL PROCESSORS WILL HAVE TO DO 2 SENDS AND 2 RECEIVES.  
 */ 

        //Synchronize end conditions
        if(my_rank==0)
        {   

            //print_debug("%s","came here");
            //u[0] = 2 - u[1]; 
            MPI_Send(&(u[n_local-1]), 1, MPI_DOUBLE, my_rank+1, 0, MPI_COMM_WORLD);
            MPI_Recv(&(u[n_local+1]), 1, MPI_DOUBLE, my_rank+1, 0, MPI_COMM_WORLD, MPI_STATUS_IGNORE);
            
        }
        else if(my_rank==nprocs-1)

        {
          //print_debug("%s","came here");
          //u[n_local+1] = 2 - u[n_local]; 
          MPI_Send(&(u[2]), 1, MPI_DOUBLE, my_rank-1, 0, MPI_COMM_WORLD);
          MPI_Recv(&(u[0]), 1, MPI_DOUBLE, my_rank-1, 0, MPI_COMM_WORLD, MPI_STATUS_IGNORE);
          
        }

        else if(my_rank!=0 && my_rank!=nprocs-1)
        {   
            //print_debug("%s","came here");
            MPI_Send(&(u[2]), 1, MPI_DOUBLE, my_rank-1, 0, MPI_COMM_WORLD);
            MPI_Recv(&u[0], 1, MPI_DOUBLE, my_rank-1, 0, MPI_COMM_WORLD, MPI_STATUS_IGNORE);
            
            MPI_Send(&(u[n_local]), 1, MPI_DOUBLE, my_rank+1, 0, MPI_COMM_WORLD);
            MPI_Recv(&u[n_local+2], 1, MPI_DOUBLE, my_rank+1, 0, MPI_COMM_WORLD, MPI_STATUS_IGNORE);

        }
        MPI_Reduce(&largest_diff_local, &largest_diff_global, 1, MPI_DOUBLE, MPI_MAX, 0, MPI_COMM_WORLD);
        MPI_Bcast(&largest_diff_global, 1, MPI_DOUBLE, 0, MPI_COMM_WORLD); 
        //
   
        if(largest_diff_global < tol)
        {
            
            break;
        
        }




    }
    ///////////////////// COMMENT THIS PART AND UNCOMMENT THE NEXT PART TO GET RESULT FOR PART(D)
        
    /*if(my_rank!=0)
    {
        //RANK 0 WILL PRINT FIRST AND ALL THE RANKS WILL BE WAITING
        MPI_Recv(&w[0], 1, MPI_DOUBLE, my_rank-1, 0, MPI_COMM_WORLD, MPI_STATUS_IGNORE);
    }
    if(my_rank==0)
    {
        start=0;
        end = n_local;
    }
    else if(my_rank==nprocs-1)
    {
        start=1;
        end = n_local+2;
    }
    else if(my_rank!=0 && my_rank!=nprocs-1)
    {
        start=1;
        end = n_local+1;
    }
    for(i=start; i<end; i++)
    {
        printf("%.19g\n", u[i]);           
    }
   
    if(my_rank != nprocs-1)
    {
        //RANK 0 WILL PRINT AND RELIVE THE RANK 1, RANK1 WILL RELIEVE THE RANK2 SO ON...(KINDA LIKE DOMINO EFFECT, 0 WILL TIP 1, 1 WILL TIP 2, SO ...ON)  
        MPI_Send(&(w[1]), 1, MPI_DOUBLE, my_rank+1, 0, MPI_COMM_WORLD);
    }

    if(my_rank==nprocs-1) //ALL THE RANKS PRINT IN ORDER BECAUSE OF PREVIOUS STEPS, LETS MAKE ONLY LAST RANK TO PRINT ITERATIONS AND INFINITY NORM 
    {
        print_debug("%g\n",largest_diff_global);
        print_debug("%d\n",iter);
    }*/
    
    ///////////////////// UNCOMMENT THIS PART AND COMMENT THE PREVIOUS PART TO GET RESULT FOR PART(D)
    if(my_rank!=0)
    {
        //RANK 0 WILL PRINT FIRST AND ALL THE RANKS WILL BE WAITING
        MPI_Recv(&w[0], 1, MPI_DOUBLE, my_rank-1, 0, MPI_COMM_WORLD, MPI_STATUS_IGNORE);
    }
    if(my_rank==0)
    {
        start=0;
        end = n_local;
    }
    else 
    {
        start=1;
        end = n_local+1;
    }
    
    printf("%.19g\n", u[start]);           
    printf("%.19g\n", u[end]);
   
    if(my_rank != nprocs-1)
    {
        //RANK 0 WILL PRINT AND RELIVE THE RANK 1, RANK1 WILL RELIEVE THE RANK2 SO ON...(KINDA LIKE DOMINO EFFECT, 0 WILL TIP 1, 1 WILL TIP 2, SO ...ON)  
        MPI_Send(&(w[1]), 1, MPI_DOUBLE, my_rank+1, 0, MPI_COMM_WORLD);
    }


    delete_array(&u);
    delete_array(&w);
    /* etc */

    MPI_Finalize();
    //return 0;
}



/*
 if(my_rank!=0)
        {   

            //print_debug("%s","came here");
             
            MPI_Send(&(u[1]), 1, MPI_DOUBLE, my_rank-1, 0, MPI_COMM_WORLD);
            MPI_Recv(&(u[0]), 1, MPI_DOUBLE, my_rank-1, 0, MPI_COMM_WORLD, MPI_STATUS_IGNORE);
            
        }
        else
        {
            u[0] = 2 - u[1];
        }
        if(my_rank!=nprocs-1)

        {
          //print_debug("%s","came here");
           
          MPI_Recv(&(u[n_local+1]), 1, MPI_DOUBLE, my_rank+1, 0, MPI_COMM_WORLD, MPI_STATUS_IGNORE);
          MPI_Send(&(u[n_local]), 1, MPI_DOUBLE, my_rank+1, 0, MPI_COMM_WORLD);
          
          
        }
        else
        {
            u[n_local+1] = 2 - u[n_local];
        }
*/

