#include "demo7.h"
#include <demo_util.h>
#include <stdio.h>
#include <mpi.h>
#include <math.h>

#define PI 3.141592653589793116

/* Integrate this over [0,1] */
double g(double zeta)
{
    double gz;

    gz =(2*PI)*(2*PI)*(zeta)*(sin(2*PI*zeta));
    //gz = zeta;
    return gz;
}

double h(double zeta)
{
    double hz;

    hz =  (2*PI)*(2*PI)*(1-zeta)*(sin(2*PI*zeta));
    //hz = 1-zeta
    return hz;
}



double integral(double limits[], double step_size, int Int1or2)

{
	double local_integral;
	int local_i;
	double local_zeta;
	local_zeta =limits[0];
	local_i=0;
	//print_debug("%f %f",limits[0],limits[1]);
	
	if(Int1or2==1) //If Int1or2==1 then we're calcing the 1st integral, else the 2nd integral.
	{	
		
		local_integral=-(g(limits[0])+g(limits[1]))/2;
		//local_integral=0;
		while(local_zeta<=limits[1])
		{	//print_debug("came here");
			local_zeta+=(double)local_i*step_size;
			local_integral+=g(local_zeta);
			local_i++;
	
		}
	}	
	else if(Int1or2==2)
	{	
		local_integral=-(h(limits[0])+h(limits[1]))/2;
		//local_integral=0;
		while(local_zeta<=limits[1])
		{	//print_debug("came here");
			local_zeta+=(double)local_i*step_size;
			local_integral+=h(local_zeta);
			local_i++;
	
		}
	}
					

	return local_integral*step_size;


}


/* Indefinite integral goes here */
double I_exact(double x)
{
    /* Use Wolframe Alpha to code indefinite integral */
    return 0;
}

void main(int argc, char** argv)
{
    /* Data arrays */
    double n_global;
    double local_bins;
    double step_size;
    double range[2];
    /* MPI variables */
    int my_rank, nprocs;

    MPI_Init(&argc, &argv);

    MPI_Comm_rank(MPI_COMM_WORLD, &my_rank);
    set_rank(my_rank);  /* Used in printing */

    MPI_Comm_size(MPI_COMM_WORLD, &nprocs);

    if (my_rank == 0)
    {        
        int p0;
        read_int(argc,argv, "-p",&p0);
        n_global = pow2(p0);     /* Number of sub-intervals used for integration */


        double a,b;
        a = 0;
        b = 1;  

        double yp = 0;
        double w = (b-a)/nprocs;
	//double range[2];
	
        int p;
        for(p = 1; p < nprocs; p++)
        {
            /* pass two values to processor p */
	    
            
            range[0] = p*w;
            range[1] = range[0] + w;

            int tag = 0;
            int dest = p;
            MPI_Send((void*) range,2,MPI_DOUBLE,dest,tag,MPI_COMM_WORLD);

        }
	range[0]=0;
	range[1]=w;
	//printf("range in proc 0 is %f %f\n",range[0],range[1]);
    }
    else
    {
        MPI_Status status;
        int count;
        int source = 0;
        int tag = 0;
        MPI_Recv((void*) range,2,MPI_DOUBLE,source,tag,MPI_COMM_WORLD,&status);  
		

    }
    
	MPI_Bcast(&n_global, 1, MPI_DOUBLE, 0, MPI_COMM_WORLD);
	

    
	local_bins = n_global/nprocs;
	 

	step_size = (range[1]-range[0])/local_bins;
	
	
	double sample_num;
	double solution[(int)n_global];
	double total_sum;
	
	for(sample_num=0;sample_num< n_global;sample_num++)
	{
		double processor_sum;
		double x_j;
		double limits[2];
		//printf("%d,%f,%f\n",my_rank,range[0],range[1]);
		total_sum =0;
		processor_sum=0;
		x_j=sample_num*(1/n_global);
		
		if(0<range[0] && range[0] < range[1] && range[1]<=x_j)//0<range[0] && range[0] < range[1] && range[1] <=x_j
		{
			limits[0]=range[0];
			limits[1]=range[1];
			processor_sum+=(1-x_j)*integral(limits,step_size,1); //Int1
			processor_sum+=0;				//Int2
			//printf("cnd1 %d,%f,%f,%f\n",my_rank,x_j,range[0],range[1]);
			
		}
		
		else if(range[0]<x_j && x_j<=range[1])//=
		{
			limits[0]=range[0];
			limits[1]=x_j;
			processor_sum+=(1-x_j)*integral(limits,step_size,1); //Int1
			

			limits[0]=x_j;
			limits[1]=range[1];
			processor_sum+=x_j*integral(limits,step_size,2);   //Int2
			//printf("cnd2 %d,%f,%f,%f\n",my_rank,x_j,range[0],range[1]);
			
		}
		
		else if(x_j<=range[0]&& range[0]<range[1] && range[1]<=1 )//
		{
			processor_sum+=0;    //Int1
			limits[0]=x_j;
			limits[1]=range[1];
			processor_sum+=(x_j)*integral(limits,step_size,2); //Int2
			//printf("cnd3 %d,%f,%f,%f\n",my_rank,x_j,range[0],range[1]);
			

		}


		MPI_Reduce(&processor_sum,&total_sum,1,MPI_DOUBLE,MPI_SUM,0,MPI_COMM_WORLD); 
		//printf("%f,%d\n",processor_sum,my_rank);
		//printf("%d\n",my_rank);		
		MPI_Barrier(MPI_COMM_WORLD);
		solution[(int)sample_num]=total_sum;
		//print_global("final_integral_solution %f %d\n",total_sum,sample_num);
		//print_global("%f,%d,%f,%d\n",total_sum,sample_num,x_j);
		print_global("%f,\n",total_sum);
		MPI_Barrier(MPI_COMM_WORLD);
		//int elems;
		//for(elems

	} 
		



    MPI_Finalize();

}
