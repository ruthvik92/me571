clc;
clear all;
%% Loop Addition
v = [1:10000];
w=[];
tic
for i=1:length(v)
    w(i)=v(i)+v(i);
end
toc
%% Loop Multiplication
w=[];
tic
for i=1:length(v)
    w(i)=v(i)*v(i);
end
toc
%% Loop Sinusoid
w=[];
tic
for i=1:length(v)
    w(i)=sin(v(i));
end
toc
%% Vectorized addition
tic
w= v+v;
toc
%% Vectorized multiplication
tic
w= v.*v;
toc
%% Vectorized Sinusoid
tic
w= sin(v);
toc