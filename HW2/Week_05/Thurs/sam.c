# include <math.h>
# include <mpi.h>
# include "demo_util.h"
# include <stdio.h>
#define PI 3.141592653589793116

double fx(double x){
    return -(2*PI)*(2*PI)*sin(2*PI*x);
}

double exact(double x){
    return sin(2*PI*x);
}

double ur(double zeta, double r){
    if(r<zeta){
        return (zeta-1)*r*fx(r);     //h(r)
    }else{
        return (r-1)*zeta*fx(r);     //g(r)
    }
}

double trap_rule(double r0, double r1, double zeta, long n){
    double h = (r1 - r0)/n;
    double ret = -0.5*(ur(zeta, r0)+ur(zeta,r1));
    double step = (r1-r0)/n;
    int i;
    for(i=0; i<n; i++){
        ret += ur(zeta,r0+step*i);
    }
    return ret*h;
}


int main(int argc, char** argv){

    double a = 0;
    double b = 1;
    double trap_total;
    double* u;    

    int rank, nprocs;

    MPI_Init(&argc, &argv);
    MPI_Comm_rank(MPI_COMM_WORLD, &rank);
    MPI_Comm_size(MPI_COMM_WORLD, &nprocs);

    //read arguments and broadcast
    long N, n;
    int p0;
    if(rank==0){
        int p, err;
        read_int(argc,argv, "-p",&p0);
        N = pow(2,p0);
    }
    MPI_Status status;
    MPI_Bcast(&N, 1, MPI_LONG, 0, MPI_COMM_WORLD);

    n = N/nprocs;

    int i;
    double zeta;
    double h = (b-a)/N;
    for(i=0; i<=N; i++){
        zeta = a + h*i;
        double sub_int = (b-a)/nprocs;
        double r0 = a + sub_int*rank;
        double r1 = r0 + sub_int;
        double trap = trap_rule(r0, r1, zeta, n);

        MPI_Reduce(&trap, &trap_total, 1, MPI_DOUBLE, MPI_SUM, 0, MPI_COMM_WORLD);
        if(rank==0){
            printf("%.19g,\n", trap_total);
        }
    }
    
    MPI_Finalize();
    return 0;
}
