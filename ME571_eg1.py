import math
g = 9.81
v0 = 30
s0 = 25

t= 4.0

s = -0.5*g*t**2 + v0*t + s0

print("Position of the ball at time: {} is: {}").format(t,s)

## time it hits the ground : set s(t) = 0 and solve

t_ground = (v0+math.sqrt(v0**2 + 2*g*s0))/g

print('Time ball hits the ground: {}').format(t_ground)

t_max = v0/g

s_max = -0.5*g*t_max**2 + v0*t_max + s0
