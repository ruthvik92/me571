#include "hmwk3.h"
#include <demo_util.h>
# include <stdio.h>
#include <mpi.h>
#include <math.h>
#include <unistd.h>
#define PI 3.14159265358979323846264338327


double utrue(double x)
{
    double u;
    double pi2;
    pi2 = 2*PI;
    u = cos(pi2*x);
    return u;
}

double rhs(double x)
{
    double fx;
    double pi2;
    pi2 = 2*PI;
    fx = -(pi2)*(pi2)*cos(pi2*x);
    return fx;
}


void main(int argc, char** argv)
{
    /* Data arrays */
    double a,b;
    int n_global;
    int n_local;
    double h;
    double range[2];
    double *x, *F, *B;

    /* Iterative variables */
    double tol;
    int kmax;

    /* Misc variables */
    int i,j,k;

    /* MPI variables */
    int my_rank, nprocs;

    /* ----------------------------------------------------------------
       Set up MPI
     ---------------------------------------------------------------- */

    MPI_Init(&argc, &argv);

    MPI_Comm_rank(MPI_COMM_WORLD, &my_rank);
    set_rank(my_rank);  /* Used in printing */
    read_loglevel(argc,argv);

    MPI_Comm_size(MPI_COMM_WORLD, &nprocs);

    /* ----------------------------------------------------------------
       Read parameters from the command line
     ---------------------------------------------------------------- */
    if (my_rank == 0)
    {        
        int m,err,loglevel;
        read_int(argc,argv, "-m", &m, &err);
        if (err > 0)
        {
            print_global("Command line argument '-m' not found\n");
            exit(0);
        }        

        read_int(argc,argv, "-kmax", &kmax, &err);
        if (err > 0)
        {
            print_global("Command line argument '--kmax' not found\n");
            exit(0);
        }

        read_double(argc,argv, "-tol", &tol, &err);
        if (err > 0)
        {
            print_global("Command line argument '--tol' not found\n");
            exit(0);
        }
        //print_global("%g\n",tol);
        n_global = pow2(m);     
    }



    MPI_Bcast(&n_global, 1, MPI_INT, 0, MPI_COMM_WORLD);
    MPI_Bcast(&kmax, 1, MPI_INT, 0, MPI_COMM_WORLD);
    MPI_Bcast(&tol, 1, MPI_DOUBLE, 0, MPI_COMM_WORLD);
    n_local = n_global/nprocs;
    h = 1.0/(n_global);
    double begin = n_local*h*my_rank; 
    double *u;
    double *r;
    double *s;
    double *pk;

    double *z; // THIS IS USED TO PRINT SEQUENTIALLY
    zeros_array(2,&z);

    //last proc has one fewer nodes
    if(my_rank==nprocs-1)
        {
            n_local-=1;
        }

   
    zeros_array(n_local+2,&u);
    zeros_array(n_local+2,&r);
    zeros_array(n_local+2,&s);
    zeros_array(n_local+2,&pk);    
    
                                
    if(my_rank==0)             
    {                          
      u[0] = 1;                
    }                         
                               
    if(my_rank==nprocs-1)      
    {                       
        u[n_local+1] = 1;  
    }
    
    int iter;
    double   largest_diff_global, largest_diff_local, xis;

    double delta_old, temp, alpha, delta_new; //temp IS USED TO SWAP, 

    xis = begin;
    for(i=1; i<n_local+1; i++)
    {
        xis += h;
        r[i] = u[i-1] - 2*u[i] + u[i+1] - h*h*rhs(xis);
        pk[i] = r[i];
        
    }
    //// FOLLOWED SAGE'S METHOD FOR COMMUNICATING
    if(my_rank!=0)
    {
        //receive left boundary
        MPI_Recv(&(pk[0]), 1, MPI_DOUBLE, my_rank-1, 0, MPI_COMM_WORLD, MPI_STATUS_IGNORE);
        //send left boundary
        MPI_Send(&(pk[1]), 1, MPI_DOUBLE, my_rank-1, 0, MPI_COMM_WORLD);
    }
    if(my_rank != nprocs-1)
    {
        //send right boundary
        MPI_Send(&(pk[n_local]), 1, MPI_DOUBLE, my_rank+1, 0, MPI_COMM_WORLD);
        //recieve right boundary
        MPI_Recv(&pk[n_local+1], 1, MPI_DOUBLE, my_rank+1, 0, MPI_COMM_WORLD, MPI_STATUS_IGNORE);
    }
    //DEFINE THE OPERATION rT*r AS delta_old
    
    
    delta_old=0;


    for(i=1; i<n_local+1; i++)
    {
        delta_old+=r[i]*r[i];
    } 


    MPI_Allreduce(&delta_old, &temp, 1, MPI_DOUBLE, MPI_SUM, MPI_COMM_WORLD);
    delta_old = temp;
    

    for(iter=0; iter<kmax; iter++)
    {
        
        for(i=1; i<n_local+1; i++)
        {
            s[i] = - pk[i-1]+ 2*pk[i] - pk[i+1];


        }

        //calculate alpha
        alpha = 0;
        for(i=1; i<n_local+1; i++)
        {
            alpha += pk[i]*s[i];
        }
        
        MPI_Allreduce(&alpha, &temp, 1, MPI_DOUBLE, MPI_SUM, MPI_COMM_WORLD);
        alpha = delta_old/temp;
        MPI_Barrier(MPI_COMM_WORLD);
        ///  UPDATE u and r 
        largest_diff_local = 0;
        for(i=1; i<n_local+1; i++)
        {   
            u[i] += alpha*pk[i];
            if(fabs(alpha*pk[i]) > largest_diff_local){largest_diff_local = fabs(alpha*pk[i]);}///TAKE THE INFINITY NORM.
            r[i] -= alpha*s[i];
        }

        
        delta_new = 0;

        for(i=1; i<n_local+1; i++)
        {
            delta_new += r[i]*r[i];
        }
        MPI_Allreduce(&delta_new, &temp , 1, MPI_DOUBLE, MPI_SUM, MPI_COMM_WORLD);
        delta_new = temp;


        // UPDATE pk
        for(i=1; i<n_local+1; i++)
        {
            pk[i] = r[i]  +  delta_new/delta_old*pk[i];
        }

        //communicate ps to ghost nodes
        if(my_rank!=0){
            //receive left boundary
            MPI_Recv(&(pk[0]), 1, MPI_DOUBLE, my_rank-1, 0, MPI_COMM_WORLD, MPI_STATUS_IGNORE);
            //send left boundary
            MPI_Send(&(pk[1]), 1, MPI_DOUBLE, my_rank-1, 0, MPI_COMM_WORLD);
        }
        if(my_rank != nprocs-1){
            //send right boundary
            MPI_Send(&(pk[n_local]), 1, MPI_DOUBLE, my_rank+1, 0, MPI_COMM_WORLD);
            //recieve right boundary
            MPI_Recv(&pk[n_local+1], 1, MPI_DOUBLE, my_rank+1, 0, MPI_COMM_WORLD, MPI_STATUS_IGNORE);
        }
        
        delta_old = delta_new; 


        MPI_Reduce(&largest_diff_local, &largest_diff_global, 1, MPI_DOUBLE, MPI_MAX, 0, MPI_COMM_WORLD);
        MPI_Bcast(&largest_diff_global, 1, MPI_DOUBLE, 0, MPI_COMM_WORLD); 
        
       
        if(largest_diff_global < tol)
        {   
            break;
        }


        //Synchronize end conditions
        if(my_rank!=0){
            //receive left boundary
            MPI_Recv(&(u[0]), 1, MPI_DOUBLE, my_rank-1, 0, MPI_COMM_WORLD, MPI_STATUS_IGNORE);
            //send left boundary
            MPI_Send(&(u[1]), 1, MPI_DOUBLE, my_rank-1, 0, MPI_COMM_WORLD);
        }
        if(my_rank != nprocs-1)
        {
            //send right boundary
            MPI_Send(&(u[n_local]), 1, MPI_DOUBLE, my_rank+1, 0, MPI_COMM_WORLD);
            //recieve right boundary
            MPI_Recv(&u[n_local+1], 1, MPI_DOUBLE, my_rank+1, 0, MPI_COMM_WORLD, MPI_STATUS_IGNORE);
        }
    }

    //print output in order
        if(my_rank==0){
            printf("%.19g\n",u[0]);
        }
        if(my_rank!=0){
            //wait for signal
            MPI_Recv(&u[0], 1, MPI_DOUBLE, my_rank-1, 0, MPI_COMM_WORLD, MPI_STATUS_IGNORE);
        }
        //print array
        for(i=1; i<n_local+1; i++){
            printf("%.19g\n",u[i]);
        }
        if(my_rank != nprocs-1){
            
            //send signal
            MPI_Send(&(u[n_local+1]), 1, MPI_DOUBLE, my_rank+1, 0, MPI_COMM_WORLD);
        }
        if(my_rank==nprocs-1)
        {
            printf("%.19g\n",u[n_local+1]);
            printf("%d\n", iter);
            printf("%.19g\n",largest_diff_global);
        }
    
        
    
    

    delete_array(&u);
    delete_array(&z);
    delete_array(&s);
    delete_array(&r);
    delete_array(&pk);
    /* etc */

    MPI_Finalize();
    //return 0;
}


