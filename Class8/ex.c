#include <stdio.h>
#include <mpi.h>

void main(int argc, char** argv)    
{
    int my_rank;

    MPI_Init(&argc, &argv);

    /* Some MPI calls here */
    MPI_Comm_rank(MPI_COMM_WORLD, &my_rank);
    printf("Hello, World from processor %d\n",my_rank);

    MPI_Finalize();

}

