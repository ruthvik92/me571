#include <mpi.h>
#include <math.h>
#include <stdio.h>  
#include <stddef.h>  /* offsetof */

typedef struct 
{
    char suits[2][10];   /* "spades","hearts","diamonds","clubs" */
    char values[2][3];  /* "A", "2", "3", ... ,"10", "J", "Q", "K" */
    int total;          /* Sum of two cards */
    float bet;          /* between $1.00 and $100.00, in inc. of $0.50$ */
} struct_player_t;

void build_player_type(MPI_Datatype *player_t)
{
    int block_lengths[4] = {26,1,1}; /* 26 blocks for type suits + values */
                                     /* 1 block each for total, bet */

    /* Set up types */
    MPI_Datatype typelist[3];
    typelist[0] = MPI_CHAR;
    typelist[1] = MPI_INT;
    typelist[2] = MPI_FLOAT;

    /* Set up displacements */
    MPI_Aint disp[3];
    disp[0] = offsetof(struct_player_t,suits);
    disp[1] = offsetof(struct_player_t,total);
    disp[2] = offsetof(struct_player_t,bet);

    MPI_Type_create_struct(3, block_lengths, disp, typelist, player_t);
    MPI_Type_commit(player_t);
}

void main(int argc, char** argv)
{
    /* Data arrays */
    int card[52];
    int round[2];
    struct_player_t player;

    /* File I/O */
    MPI_File   file;
    MPI_Status status;
    
    /* Data type */
    MPI_Datatype player_t;
    MPI_Datatype localarray;

    int rank, nprocs;
    int j;

    /* ---- MPI Initialization */
    MPI_Init(&argc, &argv);

    MPI_Comm_rank(MPI_COMM_WORLD, &rank);
    set_rank(rank);  
    read_loglevel(argc,argv);
    MPI_Comm_size(MPI_COMM_WORLD, &nprocs);

    /* Node 0*/
    if (rank == 0) {

        srand(time(0));
        for (j = 0; j < 52; j++) {
            card[j] = j+1;
        }

        /* Shuffle the deck */
        for(j = 0; j < 52; j++) {
             int r = j + (rand() % (52 - j));   
             int temp = *(card + r);
             *(card + r) = *(card + j);
             *(card + j) = temp;
              //printf("%d\n", card[j]);
         }
    }

    /* Distributes Data */
    MPI_Scatter(card, 2, MPI_INT, &round, 2, MPI_INT, 0, MPI_COMM_WORLD);
    /* Build Player Type */
    build_player_type(&player_t);
    player.total = 0;
    /* Each processor saves suits, values, and totals of their hand */
    const char suits[4][10] = {"spades","hearts","diamonds","clubs"};
    const char values[13][3] = {"A", "2", "3", "4", "5", "6", "7", "8", 
                                "9", "10", "J", "Q", "K"}; // each element is 3 bytes (last byte is NULL)

    /*Random Bet*/
    //srand(time(0));
    random_seed();
    player.bet = (2 + (rand()%200)) / 2.0;

    /*Calculating Total*/
    for(j = 0; j < 2; j++) {
        /*Value and Total */
        sprintf(player.values[j], "%s", values[(round[j]%13)]);
        if(round[j]%13 == 0) {
            player.total += 13;
        } else {
            player.total += ((round[j]%13) + 1);
        }

        /*Card Type*/
        if(round[j] <= 13){
            sprintf(player.suits[j], "%s", suits[0]); //spades
        } else if ( round[j] > 13 && round[j] <= 26) {
            sprintf(player.suits[j], "%s", suits[1]); //hearts
        } else if ( round[j] > 26 && round[j] <= 39) {
            sprintf(player.suits[j], "%s", suits[2]); //diamonds
        } else if ( round[j] > 39 && round[j] <= 52) {
            sprintf(player.suits[j], "%s", suits[3]); //clubs
        } 
    }

    /* ---- Create view for this processor into file */
    int globalsize = nprocs; // size of global array
    int localsize = 1; // size of local array 
    int starts = rank; // starting positing in the global array
    int order = MPI_ORDER_C;


    MPI_Type_create_subarray(1, &globalsize, &localsize, &starts, order, 
                              player_t, &localarray);
    
    MPI_Type_commit(&localarray);

    MPI_File_open(MPI_COMM_WORLD, "bin.out", 
                   MPI_MODE_CREATE|MPI_MODE_WRONLY,
                   MPI_INFO_NULL, &file);

    MPI_Offset offset = 0;
    MPI_File_set_view(file, offset,  player_t, localarray, 
                            "native", MPI_INFO_NULL);

    /* ---- Write out file */
    MPI_File_write_all(file, &player, localsize, player_t, MPI_STATUS_IGNORE);

    /* ---- Clean up */
    MPI_File_close(&file);
    MPI_Type_free(&localarray);
    MPI_Finalize();

}